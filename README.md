# tf-branch-example
This repo is an example of using terraform to create azure infrastructure based on environments.  
To create the infrastructure defined here the following variables must be set at runtime:  

* adminuser - the username for the created VMs
* adminpass - the admin password for the created vms  

These can be set using the command:  
terraform plan -var-file tutorial.tfvars -var adminuser=\<username\> -var adminpass=\<password\> -out=plan  
This infrastructure can then be created using:  
terraform apply plan
